import UIKit

class ParticleField: UIView {
    
    // MARK: Properties
    
    private var array:[Particle] = [Particle]()
    
    private let draggableView = UIView()
    
    private var displayLink: CADisplayLink?
    
    private let sizeOfSpread = 300
    
    private var fingureLocation = CGPoint(x: -1, y: -1)
    
    // MARK: Initialization
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = #colorLiteral(red: 0.7138998508, green: 0.09803337604, blue: 0.1416322887, alpha: 1)
        self.isMultipleTouchEnabled = true
        
        displayLink = CADisplayLink(target: self, selector: #selector(onFrame))
        displayLink?.add(to: RunLoop.main, forMode: RunLoop.Mode.default)

        draggableView.frame = CGRect(origin: self.center, size: CGSize(width: sizeOfSpread, height: sizeOfSpread))
        draggableView.backgroundColor = .clear
        draggableView.layer.cornerRadius = draggableView.frame.size.width/2
        draggableView.clipsToBounds = true
        addSubview(draggableView)
        create()
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: UIResponder
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        guard let location = touches.first?.location(in: self) else { return }
        fingureLocation = location
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesMoved(touches, with: event)
        guard let location = touches.first?.location(in: self) else { return }
        fingureLocation = location
    }
    
    override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesCancelled(touches, with: event)
        fingureLocation = CGPoint(x: -1, y: -1)
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesEnded(touches, with: event)
        fingureLocation = CGPoint(x: -1, y: -1)
        guard let location = touches.first?.location(in: self) else { return }
        draggableView.layoutIfNeeded()
        for particle in array {
            particle.resetColor()
            if draggableView.frame.contains(CGPoint(x: particle.x, y: particle.y)) {
                particle.setPartitionSpread(point: location)
            }
        }
        setNeedsLayout()
    }
    
    private func highLight(location: CGPoint) {
        draggableView.center = location
        draggableView.layoutIfNeeded()
        for particle in array {
            if draggableView.frame.contains(CGPoint(x: particle.x, y: particle.y)) {
                let distance = sqrt(((particle.x - location.x)*(particle.x - location.x) + (particle.y - location.y)*(particle.y - location.y)))
                let alpha = (200 - distance)/200
                particle.setFillColor(color: UIColor.init(white: 1, alpha: alpha + 0.3))
            } else {
                particle.resetColor()
            }
        }
        setNeedsDisplay()
    }
    
    private func dim() {
        for particle in array {
            particle.resetColor()
        }
        setNeedsDisplay()
    }
    
    // MARK: Functions
    
    private func create() {
        for _ in 0...300 {
            let x = CGFloat(arc4random_uniform(UInt32(self.frame.width)))
            let y = CGFloat(arc4random_uniform(UInt32(self.frame.height)))
            self.spawn(x: x, y: y)
        }
    }
    
    @objc private func onFrame() {
        if fingureLocation != CGPoint(x: -1, y: -1) {
            highLight(location: fingureLocation)
        } else {
            dim()
        }
        
        for i in stride(from: array.count - 1, to: -1, by: -1) {
            let particle = self.array[i]
            particle.move()
            if particle.isDead {
                particle.removeFromSuperview()
                array.remove(at: i)
                let x = CGFloat(arc4random_uniform(UInt32(self.frame.width)))
                self.spawn(x: x, y: -30)
            }
        }
        self.setNeedsDisplay()
    }
    
    private func spawn(x: CGFloat, y: CGFloat) {
        let force = random(min: -8.0, max: 8.0)
        let particle = Particle(x: x, y: y, radius: random(min: 3, max: 8))
        particle.wander = random(min: 0, max: 5.3)
        particle.drag = random(min: 0.9, max: 0.95)
        particle.theta = random(min: 0.0, max: .pi / 4)
        particle.vx = sin(particle.theta) * force
        particle.vy = cos(particle.theta) * force
        particle.fillColor = UIColor(hex: "#fffafa")
        array.append(particle)
        self.addSubview(particle)
    }
}

///MARK: -Global Function
func random() -> CGFloat {
    return CGFloat(Float(arc4random()) / Float(UINT32_MAX))
}

func random(min: CGFloat, max:CGFloat) -> CGFloat {
    return random()*(max-min)+min
}
