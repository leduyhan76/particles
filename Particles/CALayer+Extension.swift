import UIKit

extension CALayer {
    func moveTo(point: CGPoint, animated: Bool,_ autoReverse: Bool = false,_ completion: @escaping () -> ()?) {
        if animated {
            let animation = CABasicAnimation(keyPath: "position")
            CATransaction.setCompletionBlock {
                completion()
            }
            animation.fromValue = value(forKey: "position")
            animation.toValue = NSValue(cgPoint: point)
            animation.duration = 0.5
            animation.fillMode = .forwards
            animation.autoreverses = autoReverse
            animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeOut)
            self.position = point
            add(animation, forKey: "position")
        } else {
            self.position = point
        }
    }
}
