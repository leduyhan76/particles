import UIKit

class Particle: UIView {
    
    var x: CGFloat
    var y: CGFloat
    var vx: CGFloat = 0
    var vy: CGFloat = 0
    var radius: CGFloat = 20
    var isDead = true
    var wander = CGFloat(1.15)
    var theta = random(min: 0, max: 2 * .pi)
    var drag = CGFloat(0.92)
    var fillColor: UIColor? {
        didSet {
            guard let color = fillColor, isFirstSetColor else { return }
            isFirstSetColor = false
            saveColor = color
        }
    }
    
    private var saveColor: UIColor = .clear
    private var isOnAnimationTime = true
    private var isFirstSetColor = true
    private var numberOfAnimationsInView = 0

    init(x:CGFloat, y: CGFloat, radius: CGFloat) {
        self.x = x
        self.y = y
        self.radius = radius
        let frame = CGRect(x: self.x - self.radius, y: self.y - self.radius, width: radius*2, height: radius*2)
        super.init(frame: frame)
        self.backgroundColor = .white
        self.layer.cornerRadius = self.frame.size.width/2
        self.clipsToBounds = true
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setPartitionSpread(point: CGPoint) {
        numberOfAnimationsInView += 1
        isOnAnimationTime = false
        let denominator = sqrt((point.x-x)*(point.x-x) + (point.y - y)*(point.y - y))
        let a = point.x + 150*(x - point.x)/denominator
        let b = point.y + 150*(y - point.y)/denominator
        if sqrt((point.x-a)*(point.x-a) + (point.y - b)*(point.y - b)) < denominator {
            numberOfAnimationsInView -= 1
            return isOnAnimationTime = self.numberOfAnimationsInView < 1
        }
        x = a
        y = b
        self.layer.moveTo(point: CGPoint(x: self.x, y: self.y), animated: true, false, { [weak self] in
            guard let `self` = self else { return nil }
            self.numberOfAnimationsInView -= 1
            return self.isOnAnimationTime = self.numberOfAnimationsInView < 1
        })
    }
    
    func setFillColor(color: UIColor) {
        fillColor = color
    }
    
    
    func resetColor() {
        self.fillColor = self.saveColor
    }
    
    func move() {
        if !isOnAnimationTime {
            return
        }
        self.x += self.vx
        self.y += self.vy
        
        self.vx *= self.drag
        self.vy *= self.drag
        
        self.theta += random(min: -0.5, max: 0.5) * self.wander
        
        self.vx += sin(self.theta) * 0.1
        self.vy += 0.18
        self.isDead = y > UIScreen.main.bounds.size.height
        self.frame = CGRect(x: self.x - self.radius, y: self.y - self.radius, width: radius*2, height: radius*2)
        self.backgroundColor = self.fillColor
    }
}

